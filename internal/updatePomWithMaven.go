package internal

import (
	"fmt"
	"os"
	"os/exec"
)

func UpdatePomWithMaven(file *PropertiesFile, verbose bool) error {
	fmt.Println("Updating pom using maven versions plugin")
	plugin := file.VersionsPlugin
	if file.VersionsPlugin == "" {
		plugin = "org.codehaus.mojo:versions-maven-plugin:2.7"
	}

	if len(file.ModuleVersion) > 0 {
		fmt.Printf("Updating module version to %v...", file.ModuleVersion)
		cmd := exec.Command("mvn",
			fmt.Sprintf("%v:set", plugin),
			"-DgenerateBackupPoms=false",
			fmt.Sprintf("-DnewVersion=%v", file.ModuleVersion),
			"--no-transfer-progress",
		)
		err := runCommand(cmd, verbose)
		if err != nil {
			return err
		}
		fmt.Print(" done.\n")
	}

	if len(file.ParentVersion) > 0 {
		fmt.Printf("Updating parent version to %v...", file.ParentVersion)
		cmd := exec.Command("mvn",
			fmt.Sprintf("%v:update-parent", plugin),
			"-DgenerateBackupPoms=false",
			fmt.Sprintf("-DparentVersion=%v", file.ParentVersion),
			"--no-transfer-progress",
		)
		err := runCommand(cmd, verbose)
		if err != nil {
			return err
		}
		fmt.Print(" done.\n")
	}

	fmt.Printf("Updating %d properties...", len(file.Properties))
	for i, property := range file.Properties {
		cmd := exec.Command("mvn",
			fmt.Sprintf("%v:set-property", plugin),
			"-DgenerateBackupPoms=false",
			fmt.Sprintf("-Dproperty=%v", property.Name),
			fmt.Sprintf("-DnewVersion=%v", property.Value),
			"--no-transfer-progress",
		)
		fmt.Printf("Updating property %v to %v...", property.Name, property.Value)
		err := runCommand(cmd, verbose)
		if err != nil {
			return err
		}
		fmt.Printf("(%d/%d)\n", i+1, len(file.Properties))
	}
	return nil
}

func runCommand(cmd *exec.Cmd, verbose bool) error {
	if verbose {
		fmt.Printf("Command: %v\n", cmd.String())
		cmd.Stderr = os.Stderr
		cmd.Stdout = os.Stdout
		err := cmd.Run()
		if err != nil {
			fmt.Printf("\nSomething went wrong, aborting: %v\n", err)
			return err
		}
	} else {
		output, err := cmd.CombinedOutput()
		if err != nil {
			fmt.Printf("\nSomething went wrong, aborting and dumping mvn output to stderr: %v\n", err)
			print(string(output))
			return err
		}
	}
	return nil
}
