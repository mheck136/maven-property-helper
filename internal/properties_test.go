package internal

import (
	"fmt"
	"testing"
)

func TestPropertiesFile_IncrementModuleVersion(t *testing.T) {
	type fields struct {
		VersionsPlugin string
		ModuleVersion  string
		ParentVersion  string
		Properties     []*struct {
			Name  string `yaml:"name"`
			Value string `yaml:"value"`
		}
	}
	type args struct {
		partToIncrement string
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantErr  bool
		validate func(file *PropertiesFile) *string
	}{
		{
			name: "Test incrementing major",
			fields: fields{
				VersionsPlugin: "",
				ModuleVersion:  "1.0.0",
				ParentVersion:  "1.0.0",
			},
			args: struct{ partToIncrement string }{partToIncrement: "major"},
			validate: func(file *PropertiesFile) *string {
				expected := "2.0.0"
				if file.ModuleVersion != expected {
					testFailure := fmt.Sprintf("Expected new version to be %v, but got %v.", expected, file.ModuleVersion)
					return &testFailure
				}
				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			propertiesFile := &PropertiesFile{
				VersionsPlugin: tt.fields.VersionsPlugin,
				ModuleVersion:  tt.fields.ModuleVersion,
				ParentVersion:  tt.fields.ParentVersion,
				Properties:     tt.fields.Properties,
			}
			err := propertiesFile.IncrementModuleVersion(tt.args.partToIncrement)
			if (err != nil) != tt.wantErr {
				t.Errorf("IncrementModuleVersion() error = %v, wantErr %v", err, tt.wantErr)
			} else if msg := tt.validate(propertiesFile); err == nil && msg != nil {
				t.Error(msg)
			}
		})
	}
}
