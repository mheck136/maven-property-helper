package internal

import (
	"errors"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

type PropertiesFile struct {
	VersionsPlugin string `yaml:"versionsPlugin,omitempty"`
	ModuleVersion  string `yaml:"moduleVersion,omitempty"`
	ParentVersion  string `yaml:"parentVersion,omitempty"`
	Properties     []*struct {
		Name  string `yaml:"name"`
		Value string `yaml:"value"`
	} `yaml:"properties,omitempty"`
}

func (propertiesFile *PropertiesFile) Read(filePath string) error {
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(bytes, propertiesFile)

	if err != nil {
		return err
	}
	return nil
}

func (propertiesFile *PropertiesFile) Write(filePath string) error {
	bytes, err := yaml.Marshal(propertiesFile)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filePath, bytes, 0644)
	if err != nil {
		return err
	}
	return nil
}

func (propertiesFile *PropertiesFile) IncrementModuleVersion(partToIncrement string) error {
	matches, _ := regexp.MatchString("^\\d+((\\.\\d+){0,2})\\z", propertiesFile.ModuleVersion)
	if !matches {
		return errors.New(fmt.Sprintf("'%v' is not a valid version number and can't be incremented.", propertiesFile.ModuleVersion))
	}
	versionPartStrings := strings.Split(propertiesFile.ModuleVersion, ".")
	partCount := len(versionPartStrings)
	if partToIncrement == "minor" && partCount < 2 {
		return errors.New(fmt.Sprintf("'%v' doesn't have a minor part and can't be incremented.", propertiesFile.ModuleVersion))
	}
	if partToIncrement == "patch" && partCount < 3 {
		return errors.New(fmt.Sprintf("'%v' doesn't have a patch part and can't be incremented.", propertiesFile.ModuleVersion))
	}

	for i, partString := range versionPartStrings {
		partNumber, _ := strconv.Atoi(partString)
		if (partToIncrement == "major" && i == 0) ||
			(partToIncrement == "minor" && i == 1) ||
			(partToIncrement == "patch" && i == 2) {
			partNumber++
		}
		versionPartStrings[i] = strconv.Itoa(partNumber)
	}

	propertiesFile.ModuleVersion = strings.Join(versionPartStrings, ".")
	return nil
}

func (propertiesFile *PropertiesFile) SetProperty(propertyName string, propertyValue string) error {
	for _, property := range propertiesFile.Properties {
		if property.Name == propertyName {
			property.Value = propertyValue
			return nil
		}
	}
	return errors.New(fmt.Sprintf("no property %v found", propertyName))
}
